import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
const store = () => new Vuex.Store({
  state: {
    formAvailable: true,
    positions: [],
    positionsNames: [],
    newUser: {
      name: '',
      phone: '',
      email: '',
      photo: {},
      position_id: null,
    },
    mainLinks: [
      {to: '/#about', text: 'about'},
      {to: '/#relationships', text: 'relationships'},
      {to: '/#users', text: 'users'},
      {to: '/register', text: 'signUp'},
    ],
    footerLinksLists: [
      [
        {to: '#news', text: 'news'},
        {to: '#blog', text: 'blog'},
        {to: '#partners', text: 'partners'},
        {to: '#shop', text: 'shop'},
      ],
      [
        {to: '#overview', text: 'overview'},
        {to: '#design', text: 'design'},
        {to: '#code', text: 'code'},
        {to: '#collaborate', text: 'collaborate'},
      ],
      [
        {to: '#tutorials', text: 'tutorials'},
        {to: '#resources', text: 'resources'},
        {to: '#guides', text: 'guides'},
        {to: '#examples', text: 'examples'},
      ],
      [
        {to: '#faq', text: 'faq'},
        {to: '/terms', text: 'terms'},
        {to: '/terms', text: 'conditions'},
        {to: '#help', text: 'help'},
      ],
    ],
    socialLinks: [
      'facebook',
      'linkedin',
      'instagram',
      'twitter',
      'pinterest',
    ],
  },
  mutations: {
    updateName(state, name) {
      state.newUser.name = name;
    },
    updatePhone(state, phone) {
      state.newUser.phone = phone;
    },
    updateEmail(state, email) {
      state.newUser.email = email;
    },
    updatePosition(state, positionName) {
      if (positionName) {
        let positionId = state.positions.find(position => position.name == positionName).id;
        state.newUser.position_id = positionId;
      }
    },
    updatePhoto(state, photo) {
      if (photo) {
        state.newUser.photo = photo;
      }
    },
    getPositions(state) {
      fetch('https://frontend-test-assignment-api.abz.agency/api/v1/positions')
        .then(function(response) {
          return response.json();
        })
        .then((data) => {
          data.positions.forEach(position => {
            state.positions.push(position);
            state.positionsNames.push(position.name);
          })
        })
        .catch(() => {
          state.formAvailable = false;
        });
    },
  },
})

export default store;
