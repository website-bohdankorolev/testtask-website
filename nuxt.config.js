require('dotenv').config();

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'task3',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'robots', content: 'noindex, nofollow' },
      { name: 'author', content: 'Frontend_Bohdan_K' },
      { name: 'description', content: 'We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository.' +
          'Please be patient, we consider and respond to every application that meets minimum requirements. We look forward to your submission.'
      },
      { name: 'keywords', content: 'test assignment, for frontend developer, frontend developer position, developer position, acquainted, let\'s get acquainted, relationships, relationships with web-development, users, cheerful users' },
      { name: 'twitter:site', content: 'Task #3' },
      { name: 'twitter:title', content: 'Main page' },
      { name: 'twitter:description', content: 'Test assignment for Frontend Developer position' },
      { name: 'twitter:creator', content: 'Frontend_Bohdan_K' },
      { name: 'twitter:domain', content: 'task3-test2020bohdan-k.abzdev2.com' },
      { name: 'og:title', content: 'Main page' },
      { name: 'og:description', content: 'Test assignment for Frontend Developer position' },
      { name: 'og:url', content: 'https://task3-test2020bohdan-k.abzdev2.com/' },
      { name: 'og:site_name', content: 'Task #3' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Overpass:wght@600&family=Source+Sans+Pro:wght@400;700&display=swap' }
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~/plugins/i18n.js',
  ],
  buildModules: [
    [
      '@nuxtjs/vuetify',
      {
        defaultAssets: {
          icons: false,
        },
      },
    ],
    // '@nuxtjs/gtm',
  ],
  // gtm: {
  //   id: 'GTM-TXLNQPG'
  // }
}

